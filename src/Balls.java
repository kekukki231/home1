import java.util.Arrays;

public class Balls {

    enum Color {green, red};

    public static void main(String[] param) {
        Color[] colors = {Color.red, Color.green, Color.red, Color.green, Color.green, Color.green, Color.red};
        //reorder(colors);
        //System.out.println(Arrays.toString(colors));
    }

    public static void reorder(Color[] balls) {
        int listLength = balls.length;
        Color[] tempColors = new Color[listLength];
        int reds = 0;
        int greens = 0;
        for (int i = 0; i < listLength; i++) {
            if (balls[i].equals(Color.red)) {
                tempColors[reds] = Color.red;
                reds++;
            } else {
                tempColors[listLength - 1 - greens] = Color.green;
                greens++;
            }
        }
        for (int i = 0; i < listLength; i++) {
            balls[i] = tempColors[i];
        }
    }
}

